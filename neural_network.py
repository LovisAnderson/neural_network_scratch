import numpy as np
from abc import ABC, abstractmethod


class Layer(ABC):
    learnable = False
    input = None

    @abstractmethod
    def forward(self, x):
        pass

    @abstractmethod
    def backpropagate(self):
        pass


class Activation(ABC):

    @abstractmethod
    def __call__(self, x):
        pass

    @abstractmethod
    def derivative(self, x):
        pass


class NoActivation(Activation):
    def __call__(self, x):
        return x

    def derivative(self, x):
        return np.ones(x.shape)


class NeuronLayer(Layer):
    def __init__(self, weights, biases, activation=None):
        self.weights = weights  # for each neuron a row of weights
        self.biases = biases  # for each neuron a row of biases
        self.learnable = True
        self.activation = activation if activation is not None else NoActivation()

    def forward(self, x):
        self.input = x
        return self.activation(np.dot(self.weights, x) + self.biases)

    def backpropagate(self, y, learning_rate=0.01):
        assert self.input is not None, "You need to do a forward pass first in order to backpropagate!"
        backwards = y * self.activation.derivative(np.dot(self.weights, self.input) + self.biases) * self.weights
        self.weights -= np.dot(backwards, self.input) * learning_rate
        self.input = None
        return backwards


class ReLuActivation(Activation):

    def __call__(self, x):
        x[x < 0] = 0
        return x

    def derivative(self, x):
        x[x <= 0] = 0
        x[x > 0] = 1
        return x


class NeuralNetwork:
    def __init__(self, layers, loss_layer):
        self.layers = layers
        self.loss_layer = loss_layer

    def forward(self, x, y_true=None):
        for layer in self.layers:
            x = layer.forward(x)
        if y_true is not None:
            loss = self.loss_layer.forward(x, y_true)
        else:
            loss = None
        return x, loss


    def backpropagate(self, learning_rate=0.1):
        y = self.loss_layer.backpropagate()
        for layer in reversed(self.layers):
            if layer.learnable:
                y = layer.backpropagate(y, learning_rate=learning_rate)
            else:
                y = layer.backpropagate(y)


class MeanSquaredError(Layer):

    def __init__(self):
        self.input = (None, None)
        self.learnable = False

    def forward(self, y_pred, y_true):
        self.input = (y_pred, y_true)
        squared_error = np.square(y_true - y_pred)
        return np.mean(squared_error)

    def backpropagate(self, y=1):
        assert all(a is not None for a in self.input), "You need to do a forward pass first in order to backpropagate!"
        backwards = 2 * (self.input[0] - self.input[1])
        self.input = (None, None)
        return backwards
import pytest
from neural_network import *
import numpy as np


x_test_1 = np.array([1., 1.])


@pytest.fixture
def simple_network():
    weights_1 = np.array([[0.5, 0.5], [0.5, 0.5]])
    biases = np.array([0, 0])
    relu = ReLuActivation()
    layer_1 = NeuronLayer(weights=weights_1, biases=biases, activation=relu)
    weights_2 = np.array([[1., 0.], [0., 1.]])
    layer_2 = NeuronLayer(weights=weights_2, biases=biases, activation=relu)

    loss_layer = MeanSquaredError()
    nn = NeuralNetwork([layer_1, layer_2], loss_layer=loss_layer)
    return nn


def test_forward(simple_network):

    y, loss = simple_network.forward(x_test_1)
    assert np.array_equal(x_test_1, y)


def test_backpropagation(simple_network):
    y_true = np.array([0.5, 0.5])
    y = simple_network.forward(x_test_1, y_true=y_true)

    simple_network.backpropagate(learning_rate=1.)
    pass